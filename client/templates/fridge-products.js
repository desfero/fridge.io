Template.fridgeProducts.helpers({
    fridgeReady: function() {
        return Router.current().fridgeHandle.ready();
    },

    fridgeCategories: function() {
        var result = [];

        var fridgeId = Router.current().params._id;
        var products = Fridges.findOne({
            _id: fridgeId
        }).products;
        if (products && products.length) {
            var result = lodash.chain(Products.find().fetch()).filter(function(item) {
                return products.indexOf(item._id) !== -1
            }).map('cat').uniq().sortBy().value();
        }

        return result;
    },

    fridgeProducts: function(cat) {
        var result = [];

        var fridgeId = Router.current().params._id;
        var products = Fridges.findOne({
            _id: fridgeId
        }).products;

        if (products && products.length) {
            var result = lodash.chain(Products.find({
                cat: cat
            }).fetch()).filter(function(item) {
                return products.indexOf(item._id) !== -1
            }).value();
        }

        return result;
    }
});

var itemToDelete;

Template.fridgeProducts.events({
    'click .fridge-product': function(event) {

        var message = "Czy jesteś pewny że nie posiadasz" + this.name + " w swojej lodówce?";
        if (confirm(message)) {
            itemToDelete = this._id;

            var fridgeId = Router.current().params._id;
            var products = Fridges.findOne({
                _id: fridgeId
            }).products;

            if (products && products.length) {
                lodash.remove(products, function(n) {
                    return n === itemToDelete;
                });
            }

            Fridges.update({
                _id: fridgeId
            }, {
                $set: {
                    products: products
                }
            });
        }
    },


});
