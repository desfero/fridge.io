var EDITING_KEY = 'editingList',
    IS_PRODUCTS_LIST_OPENED = 'isProductsListOpened',
    IS_FRIDGE_OPTIONS_OPENED = 'isFridgeOptionsOpened',
    PRODUCTS_LIST_FILTER = 'productsListFilter';

Session.setDefault(EDITING_KEY, false);
Session.setDefault(IS_PRODUCTS_LIST_OPENED, false);
Session.setDefault(IS_FRIDGE_OPTIONS_OPENED, false);

Session.setDefault(PRODUCTS_LIST_FILTER, "");

// Track if this is the first time the list template is rendered
var firstRender = true;
var listRenderHold = LaunchScreen.hold();
listFadeInHold = null;

Template.fridgeShow.onRendered(function() {


    if (firstRender) {
        // Released in app-body.js
        listFadeInHold = LaunchScreen.hold();

        // Handle for launch screen defined in app-body.js
        listRenderHold.release();

        firstRender = false;
    }

    this.find('.js-title-nav')._uihooks = {
        insertElement: function(node, next) {
            $(node)
                .hide()
                .insertBefore(next)
                .fadeIn();
        },
        removeElement: function(node) {
            $(node).fadeOut(function() {
                this.remove();
            });
        }
    };
});

Template.fridgeShow.helpers({
    editing: function() {
        return Session.get(EDITING_KEY);
    },

    showProductsList: function() {
        return Session.get(IS_PRODUCTS_LIST_OPENED);
    },

    showFridgeOptions: function() {
        return Session.get(IS_FRIDGE_OPTIONS_OPENED);
    },

    showModalCloseButton: function() {
        return Session.get(IS_FRIDGE_OPTIONS_OPENED) || Session.get(IS_PRODUCTS_LIST_OPENED);
    },

    productsCount: function() {
        var count = 0,
            currentFridge = this;

        if (currentFridge.products) {
            count = currentFridge.products.length;
        }
        return count;
    },

    categories: function() {
        var filter = Session.get(PRODUCTS_LIST_FILTER);
        var selector = {};

        if (filter && filter.length) {
            var regExp = new RegExp(filter, 'i');
            selector.name = regExp;
        }

        var result = lodash.chain(Products.find(selector).fetch()).map('cat').uniq().sortBy().value();
        return result;
    },

    products: function(cat) {
        var filter = Session.get(PRODUCTS_LIST_FILTER);
        var selector = {
            cat: cat
        };

        if (filter && filter.length) {
            var regExp = new RegExp(filter, 'i');
            selector.name = regExp;
        }

        return Products.find(selector);
    },
});

var editList = function(list, template) {
    Session.set(EDITING_KEY, true);

    // force the template to redraw based on the reactive change
    Tracker.flush();
    template.$('.js-edit-form input[type=text]').focus();
};

var saveList = function(list, template) {
    Session.set(EDITING_KEY, false);
    Fridges.update(list._id, {
        $set: {
            name: template.$('[name=name]').val()
        }
    });
}
Template.fridgeShow.events({
    'keyup #products-list-filter': function(event) {
        return Session.set(PRODUCTS_LIST_FILTER, $('#products-list-filter').val());
    },

    'click .product': function(event) {
        var fridgeId = Router.current().params._id;
        var fridge = Fridges.update(fridgeId, {
            $addToSet: {
                products: this._id
            }
        });
    },

    'click .js-add-product-button': function(event) {
        Session.set(IS_PRODUCTS_LIST_OPENED, true);
        Session.set(IS_FRIDGE_OPTIONS_OPENED, false);
    },

    'click .js-close-modal-button': function(event) {
        Session.set(IS_PRODUCTS_LIST_OPENED, false);
        Session.set(IS_FRIDGE_OPTIONS_OPENED, false);
    },

    'click .js-fridge-options-icon': function(event) {
        Session.set(IS_PRODUCTS_LIST_OPENED, false);
        Session.set(IS_FRIDGE_OPTIONS_OPENED, !Session.get(IS_FRIDGE_OPTIONS_OPENED));
    },

    'click .js-cancel': function() {
        Session.set(EDITING_KEY, false);
    },

    'keydown input[type=text]': function(event) {
        // ESC
        if (27 === event.which) {
            event.preventDefault();
            $(event.target).blur();
        }
    },

    'blur input[type=text]': function(event, template) {
        // if we are still editing (we haven't just clicked the cancel button)
        if (Session.get(EDITING_KEY))
            saveList(this, template);
    },

    'submit .js-edit-form': function(event, template) {
        event.preventDefault();
        saveList(this, template);
    },

    // handle mousedown otherwise the blur handler above will swallow the click
    // on iOS, we still require the click event so handle both
    'mousedown .js-cancel, click .js-cancel': function(event) {
        event.preventDefault();
        Session.set(EDITING_KEY, false);
    },

    'click .js-edit-list': function(event) {
        editList(this);
    }
});
