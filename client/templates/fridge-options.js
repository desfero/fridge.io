Template.fridgeOptions.helpers({
    isPrivate: function() {
        return (this.users && this.users.length !== 0) ? true : false;
    },

    getUsers: function() {
        var usersId = this.users;
        return Meteor.users.find({
            _id: {
                $in: usersId
            }
        }).fetch();
    }
});

Template.fridgeOptions.events({
    'change .js-fridge-private-checkbox': function(event) {
        if (event.target.checked) {
            Fridges.update(this._id, {
                $set: {
                    users: [Meteor.userId()]
                }
            });
        } else {
            Fridges.update(this._id, {
                $unset: {
                    users: ""
                }
            });
        }
    },
    'click .js-fridge-delete': function(event) {
        deleteFridge(this);
    },
    'click .js-fridge-add-user': function(event) {
        var email = $('#add-fridge-user').val();
        var that = this;

        Meteor.call('getUserId', email, function(err, id) {
            if (id == null) {
                var message = "Użytkownik z adresem " + email + " nie istnieje.";
                alert(message);
            } else {
                Fridges.update(that._id, {
                    $push: {
                        users: id
                    }
                });

                $('#add-fridge-user').val("");

                // Little hack for updating subscription for users, whick have access to this fridge
                Meteor.disconnect();
                Meteor.reconnect();
            }
        });
    },
    'click .js-fridge-delete-user': function(event) {
        var fridgeId = Router.current().params._id;
        Fridges.update({
            _id: fridgeId
        }, {
            $pull: {
                users: this._id
            }
        });
    }
});

function deleteFridge(fridge) {
    var message = "Czy jesteś pewny że chcesz usunąć " + fridge.name + "?";
    if (confirm(message)) {
        Fridges.remove(fridge._id);

        Router.go('home');
        return true;
    } else {
        return false;
    }
};
