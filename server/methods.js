Meteor.startup(function() {
    Meteor.methods({
        getUserId: function(email) {
            check(email, String);
            var result = _.filter(Meteor.users.find().fetch(), function(user) {
                return user.emails[0].address === email;
            });

            if (result.length === 1) {
                return result[0]._id;
            }

            return null;
        }
    });
});
