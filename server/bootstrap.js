// if the database is empty on server start, create some sample data.
Meteor.startup(function() {
    if (Fridges.find().count() === 0) {
        var data = [{
            name: "Lodówka akademik",
            products: []
        }, {
            name: "W domu",
            products: []
        }];

        _.each(data, function(fridge) {
            Fridges.insert({
                name: fridge.name,
                products: fridge.products
            });
        });

    };

    if (Products.find().count() === 0) {
        Products.insert({
            src: '/img/foods/apple.png',
            name: 'Jabłka',
            cat: 'Owoce',
        });
        Products.insert({
            src: '/img/foods/chicken.png',
            name: 'Kurczak',
            cat: 'Mięso'
        });
        Products.insert({
            src: '/img/foods/fish.png',
            name: 'Ryba',
            cat: 'Ryba'
        });
        Products.insert({
            src: '/img/foods/hamburger.png',
            name: 'Hamburger',
            cat: 'Fast-food'
        });
        Products.insert({
            src: '/img/foods/meet.png',
            name: 'Mięso',
            cat: 'Mięso'
        });
        Products.insert({
            src: '/img/foods/milk.png',
            name: 'Mleko',
            cat: 'Przetwory mleczne'
        });
        Products.insert({
            src: '/img/foods/mustard.png',
            name: 'Musztarda',
            cat: 'Sosy'
        });
        Products.insert({
            src: '/img/foods/raspberry.png',
            name: 'Maliny',
            cat: 'Owoce'
        });
        Products.insert({
            src: '/img/foods/salami.png',
            name: 'Salami',
            cat: 'Mięso'
        });
        Products.insert({
            src: '/img/foods/tomato.png',
            name: 'Pomitory',
            cat: 'Warzywa'
        });
        Products.insert({
            src: '/img/foods/banana.png',
            name: 'Banany',
            cat: 'Owoce'
        });
        Products.insert({
            src: '/img/foods/cake.png',
            name: 'Ciasto',
            cat: 'Słodycze'
        });
        Products.insert({
            src: '/img/foods/carrot.png',
            name: 'Marchewki',
            cat: 'Warzywa'
        });
        Products.insert({
            src: '/img/foods/cheese.png',
            name: 'Ser',
            cat: 'Produkty mleczne'
        });
        Products.insert({
            src: '/img/foods/cheeseburger.png',
            name: 'Cheeseburger',
            cat: 'Fast-food'
        });
        Products.insert({
            src: '/img/foods/chili_peppers.png',
            name: 'Papryka chili',
            cat: 'Warzywa'
        });
        Products.insert({
            src: '/img/foods/chocolate.png',
            name: 'Czekolada',
            cat: 'Słodycze'
        });
        Products.insert({
            src: '/img/foods/donut.png',
            name: 'Donat',
            cat: 'Słodycze'
        });
        Products.insert({
            src: '/img/foods/egg.png',
            name: 'Jajka',
            cat: 'Inne'
        });
        Products.insert({
            src: '/img/foods/eggplant.png',
            name: 'Bakłażan',
            cat: 'Warzywa'
        });
        Products.insert({
            src: '/img/foods/beer.png',
            name: 'Piwo',
            cat: 'Alkohole'
        });
        Products.insert({
            src: '/img/foods/garlic.png',
            name: 'Czosnek',
            cat: 'Warzywa'
        });
        Products.insert({
            src: '/img/foods/gooseberry.png',
            name: 'Agrest',
            cat: 'Owoce'
        });
        Products.insert({
            src: '/img/foods/grapes.png',
            name: 'Winogron',
            cat: 'Owoce'
        });
        Products.insert({
            src: '/img/foods/hot_dog.png',
            name: 'Hod dog',
            cat: 'Fast-food'
        });
        Products.insert({
            src: '/img/foods/ice_cream.png',
            name: 'Lody',
            cat: 'Słodycze'
        });
        Products.insert({
            src: '/img/foods/ketchup.png',
            name: 'Ketczup',
            cat: 'Sosy'
        });
        Products.insert({
            src: '/img/foods/lemon.png',
            name: 'Cytryna',
            cat: 'Owoce'
        });
        Products.insert({
            src: '/img/foods/maize.png',
            name: 'Kukurudza',
            cat: 'Warzywa'
        });
        Products.insert({
            src: '/img/foods/mayonnaise.png',
            name: 'Majonez',
            cat: 'Sosy'
        });
        Products.insert({
            src: '/img/foods/mushroom.png',
            name: 'Grzyby',
            cat: 'Warzywa'
        });
        Products.insert({
            src: '/img/foods/pear.png',
            name: 'Grzuszka',
            cat: 'Owoce'
        });
        Products.insert({
            src: '/img/foods/potato.png',
            name: 'Ziemniaki',
            cat: 'Warzywa'
        });
        Products.insert({
            src: '/img/foods/pepper.png',
            name: 'Papryka',
            cat: 'Warzywa'
        });
        Products.insert({
            src: '/img/foods/pineapple.png',
            name: 'Ananas',
            cat: 'Owoce'
        });
        Products.insert({
            src: '/img/foods/pizza.png',
            name: 'Pizza',
            cat: 'Fast-food'
        });
        Products.insert({
            src: '/img/foods/plum.png',
            name: 'Śliwka',
            cat: 'Owoce'
        });
        Products.insert({
            src: '/img/foods/pretzels.png',
            name: 'Precelki',
            cat: 'Przekąski'
        });
        Products.insert({
            src: '/img/foods/pumpkin.png',
            name: 'Dynia',
            cat: 'Owoce'
        });
        Products.insert({
            src: '/img/foods/soup.png',
            name: 'Zupa',
            cat: 'Obiad'
        });
        Products.insert({
            src: '/img/foods/sausage.png',
            name: 'Kiełbasa',
            cat: 'Mięso'
        });
        Products.insert({
            src: '/img/foods/strawberry.png',
            name: 'Truskawka',
            cat: 'Owoce'
        });
        Products.insert({
            src: '/img/foods/watermelon.png',
            name: 'Arbuz',
            cat: 'Owoce'
        });
        Products.insert({
            src: '/img/foods/wine.png',
            name: 'Wino',
            cat: 'Alkohole'
        });
    }
});
