Meteor.publish('publicFridges', function() {
    return Fridges.find({
        users: {
            $exists: false
        }
    });
});

Meteor.publish('privateFridges', function() {
    if (this.userId) {
        return Fridges.find({
            users: this.userId
        });
    } else {
        this.ready();
    }
});

Meteor.publish('fridge', function(id) {
    check(id, String);
    return Fridges.find({
        _id: id
    });
});

Meteor.publish('products', function() {
    return Products.find();
});

Meteor.publish('fridgeUsers', function(id) {
    check(id, String);
    var fridge = Fridges.findOne({
        _id: id
    });
    if (fridge && fridge.users && fridge.users.length !== 0) {
        return Meteor.users.find({
            _id: {
                $in: fridge.users
            }
        }, {
            fields: {
                _id: 1,
                emails: 1
            }
        });
    } else {
        this.ready();
    }
});
