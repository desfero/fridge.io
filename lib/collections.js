Fridges = new Mongo.Collection('fridges');

Fridges.defaultName = function() {
    var nextLetter = 'A',
        nextName = 'Fridge ' + nextLetter;
    while (Fridges.findOne({
            name: nextName
        })) {
        // not going to be too smart here, can go past Z
        nextLetter = String.fromCharCode(nextLetter.charCodeAt(0) + 1);
        nextName = 'Fridge ' + nextLetter;
    }

    return nextName;
};

Products = new Mongo.Collection('products');
